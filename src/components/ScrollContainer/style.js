import styled from "styled-components";

export const Wrapper = styled.div`
  scroll-snap-type: y mandatory;
  overflow-y: scroll;
  height: 100vh;
  width: 100%;

  &::-webkit-scrollbar {
    display: none;
  }
`;
