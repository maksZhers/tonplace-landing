import { Wrapper } from "./style";

const ScrollContainer = (props) => {
  const { children } = props;

  return <Wrapper>{children}</Wrapper>;
};

export default ScrollContainer;
